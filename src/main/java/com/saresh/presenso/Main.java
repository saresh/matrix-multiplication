package com.saresh.presenso;


import java.security.InvalidParameterException;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        if (args.length == 0){
            throw new InvalidParameterException("Please provide matrix size");
        }
        int matrixSize;
        try {
            matrixSize = Integer.parseInt(args[0]);
        } catch (NumberFormatException ex){
            throw new InvalidParameterException("Please provide correct matrix size");
        }

        if (matrixSize < 1 || matrixSize > 10000) {
            throw new InvalidParameterException("Matrix size should be between 1 and 10000");
        }

        int[][] a = new int[matrixSize][matrixSize];
        int[][] b = new int[matrixSize][matrixSize];
        fillMatrix(a);
        fillMatrix(b);

        long startNP = System.currentTimeMillis();
        MatrixMultiplicator.multiply(a, b);
        long endNP = System.currentTimeMillis();
        System.out.println("Non parallel multiplication: " + (endNP - startNP));

        long startP = System.currentTimeMillis();
        MatrixMultiplicator.multiplyParallel(a, b);
        long endP = System.currentTimeMillis();
        System.out.println("Parallel multiplication: " + (endP - startP));
    }


    private static void fillMatrix(int[][] matrix) {
        Random random = new Random();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                matrix[i][j] = random.nextBoolean() ? 1 : 0;
            }
        }
    }
}
