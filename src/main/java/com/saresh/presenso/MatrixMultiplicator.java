package com.saresh.presenso;

import java.security.InvalidParameterException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class MatrixMultiplicator {

    public static int[][] multiply(int[][] matrixA, int[][] matrixB) {
        if (!areMatricesMultiplicable(matrixA, matrixB)){
            throw new InvalidParameterException("Provided matrices cannot be multiplied");
        }
        int[][] result = new int[matrixA.length][matrixB[0].length];
        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result[0].length; j++) {
                result[i][j] = getResultValue(i, j, matrixA, matrixB);
            }
        }
        return result;
    }

    public static int[][] multiplyParallel(int[][] matrixA, int[][] matrixB) {
        if (!areMatricesMultiplicable(matrixA, matrixB)){
            throw new InvalidParameterException("Provided matrices cannot be multiplied");
        }
        int processorsCount = Runtime.getRuntime().availableProcessors();
        ExecutorService exec = Executors.newFixedThreadPool(processorsCount);
        int[][] result = new int[matrixA.length][matrixB[0].length];
        try {
            for (int i = 0; i < matrixA.length; i++) {
                for (int j = 0; j < matrixB[0].length; j++) {
                    final int row = i;
                    final int col = j;
                    exec.execute(() -> result[row][col] = getResultValue(row, col, matrixA, matrixB));
                }
            }
        } finally {
            exec.shutdown();
        }
        try {
            exec.awaitTermination(600, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static boolean areMatricesMultiplicable(int[][] matrixA, int[][] matrixB){
        return matrixA[0].length == matrixB.length;
    }

    private static int getResultValue(int i, int j, int[][] matrixA, int[][] matrixB) {
        int val = 0;
        for (int k = 0; k < matrixA[0].length; k++) {
            val = (val + (matrixA[i][k] * matrixB[k][j]) % 2) % 2;
        }
        return val;
    }
}
